## ABOUT

Lets say 15 years ago (may be a little more) I was thinking why in the era of upcoming internet we still can listen radios from our city nothing more? When computers became our best friends and everybody starts to listen music on it using headphones. Before smartphones spreads wide here was an era of mp3 players. We all know what is bad in it: nobody updates it, you have to choose what to listen and so on. I keep thinking why in the era of fast mobile internet we still have no 3G Radios (it was 2007 in Novosibirsk, Russia we got 3mbps on 3G mostly in every corner of the city)?

Now 2019 are on. And we still have the problem to choose what to listen. We lost intent called "browsing" when we can tune our radio from one station to another to find a station correspondent to our mood. And here is two simply answers "why":

* we need some physical device with easy and understandable interactions
* internet radios aren't something you can "take and use": you need to find it, you have to spend one browser window for it and suppose you won't use computer speakers for something else

But we are people. So I decide to create some physical object with easy and understandable interation which will consume internet radios in the way where computer (in its human understanding) is not needed.

## History of fails

1. Tried to use variable resistors connected to DAC on MCP3008 with Adafuit driver (andlater with others ones). It is very good on the move: smooth and fast for TUNE and understandable bordered for VOLUME. But I found that here is no easy way to make value stable on MCP when nobody touch the regulators. I mean I put capacitors to power line, added pull-up resistors, tried to write some code to avaid spontaneous migration... nothing helps. So I switched to rotary encoders.
2. Tried to use 2.3" LCD SPI screen. It looks great on the start but on very first box' prototype the image become broken. Well, ehat a.. so I tried to remove it and gave some "movie" out of pictures on it, ~10% of pictures was broken, so I decide to switch to "normal" screen (salvaged from old laptop + hdmi converter)
3. I found +5v on the HDMI converter plate, but as I took it to power up my Raspberry, converter gone too hot. So I change the power schema: add DC-DC converter from +12v (right from input of HDMI converter) and this works fine now.
4. Not solved right now: As I use Raspberry Pi 2 for this project, I've add USB WiFi dongle to network connection. PROS: It is much more stable in connection in distance, CONS: it gone to sleep after while. Makes me crazy.

## A box

I was thinking about two points: how to make it in classical style and keep it looking as a modern device from XXI century. Before I stop on Art Nouveau, I supposed to use Art Deco or Bauhaus, but as far as I lost small screen this idea gone and now it is Nouveau. A box still is under development. Hope to proceed fast.

## Prereq

Before create all in mechanics you need to install some software. What do you need to make it all work:

1. apt install unclutter - unclutter is a software which helps to remove mouse pointer from screen (what is looks great!)
2. omxplayer must be installed in system - in Raspberrian it is present by default
3. install modules: cv2, numpy, PIL via PIP
4. stations can be found here: https://www.internet-radio.com/stations/rock/

Stations in site are present as PLS files. It is text file, if you will open it, you will find a Title and URLs of the station. Sometimes here more that one URL so choose any to add onto array.

## Materials

1. You need HDMI monitor (better to use old laptop's one)
2. Note row 207: '-o', 'hdmi' means to send sound to HDMI (for external USB SoundCard try to use: '-o', 'alsa:hw:1,0')
3. ANY Raspberry is good
4. ANY Rotary Encoders is good

## Settings

Sorry, guys, here was no time to create nornal setting section. Here are two sections. First is at first rows:

1. PINs of encoders
2. Screen size in pixels
3. Name for file which will store values (to resume on start)

Pins. first group is for TUNE encoder, second - VOLUME. Please, note, if rotation goes to wrong side, possible you mess with pin numbers. I recoment to use 'free' ping which will never used for other protocols (like I2C or SPI). I use these 'cause it organised in two compact groups.

Second part of settings are hidden inside createInitialPicture(), all of 'em you have to tune according to your screen size. Here are:

1. max_title - maximal lenght of station's title (program will cut after this symbol)
2. fnt - you have to look where your fonts' folder is and which font to use
3. fill=(0,0,0) - tune colors for anything

## Manual

No need to describe manuals, but:

* rotate TUNE to move white line to tune to desired station
* rotate VOLUME to make louder or quiter
* press VOLUME knob to MUTE

Have fun.

My current prototype: https://vimeo.com/333934518