import sys
import subprocess
import threading
import cv2
import numpy
import json

from time 			import sleep
from PIL 			import Image, ImageDraw, ImageFont

from RPi 			import GPIO

'''
Things you have to know:
	1. How to hide mouse pointer from screen: "unclutter -idle 0.1 -root" (don't forget to apt install unclutter)
	2. The omxplayer playser have to be installed in system
'''

# Section of pins of Tune Encoder
clk 	= 22	# CLK pin
dt  	= 27	# DT pin

# Section of pins of Volume Encoder
vpp 	= 16	# SW pin MUTE
vclk 	= 21	# CLK pin
vdt 	= 20	# DT pin

GPIO.setmode(GPIO.BCM)
GPIO.setup([clk, dt], 			GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup([vpp, vclk, vdt], 	GPIO.IN, pull_up_down=GPIO.PUD_UP)

# This is size of my non-test screen I got from Oleg Borichev
screensize 	= (768, 1366) # h, w

# Initial color for background, this have to be amber
bgcolor 	= (235, 171, 0)

# Position of the white line on the screen
position 	= 0
# Initial volume status
encvolume 	= 0
# File with encoders
encodersfile= '.radioencodersvalues'

# This is trick to solve 'wrong' direction of encoders
judgeencoder = {'tune':[], 'volume':[]}

# List of stations, best to get 'em from here: https://www.internet-radio.com/stations/rock/
#	Format:
#		- Name
#		- URL of stream
#		- Start point will determined on picturing (will work for tuner as label)
#		- Stop point (same as start) both leave zero
#		- Start VOLUME SHIFT (+ is station too quite, - if too loud) in 1/100 db
#
stations 	= [					# 15 is max for this screen
		("Rock Radio Beograd",							"http://109.206.96.95:8025/",					0, 0, 0),
		("Classic Rock Florida HD",						"http://198.58.98.83:8258/stream",				0, 0, 0),
		("Jazz Lounge",									"http://51.255.235.165:5068/stream",			0, 0, 200),
		("Smooth Jazz Florida",							"http://us4.internet-radio.com:8266/stream",	0, 0, 0),
		("MEGATON CAFE RADIO",							"http://us2.internet-radio.com:8443/stream",	0, 0, 100),
		("LOUNGE-RADIO.COM - swiss made",				"http://77.235.42.90:80/",						0, 0, -100),
		("Venice Classic Radio Italia",					"http://174.36.206.197:8000/stream",			0, 0, 0),
		("Classical WETA 90.9 FM - Washington D.C.",	"http://38.100.128.106:8000/fmlive",			0, 0, 0),
		("Sunshine 106.8",								"http://178.32.62.172:8087/stream",				0, 0, 0),
		("XRDS.fm Clarksdale, Mississippi",				"http://198.58.106.133:8321/stream",			0, 0, 0)
	]


# Create INITIAL picture as background for "Indicator". 
#	The Idea: check max row length and assume position for line.
#
#	Used:
#		- stations' list
#		- background color
#
#	Return: initial picture in PIL format!
#		- ALSO modifies stations list with adding borders of the bars to detect "tune"
#
#	Limitations:
#		- 15 stations max
#		- 50 character in title
#	please, understand these numbers from your thinking of beauty and screen size!
#
def createInitialPicture () :
	global bgcolor
	global stations
	global screensize

	# MAX symbols in TITLE
	max_title = 50

	# Creating background and initial image
	image 	= Image.new('RGB', screensize, color = bgcolor)
	draw 	= ImageDraw.Draw(image)

	# Defining step for stations (to distribute on image)
	step = screensize[1] / (len(stations) + 1)

	# Defining font (note to folder!)
	fnt = ImageFont.truetype('/usr/share/fonts/truetype/piboto/PibotoCondensed-Regular.ttf', 30)

	# Defining MAX length for station' bar BUT cannot be closer than 80% if screen sort side
	max_W = 0
	for (rid, station) in enumerate(stations, 1) :
		title = station[0]
		title = title[:max_title]
		(width, height) = fnt.getsize(title)
		if max_W < 15+width+10 : 
			max_W = 15+width+10
	if max_W < int(screensize[0] * 0.80) : max_W = int(screensize[0] * 0.80)

	# Draw lines, bars and titles' text for entire picture
	for (rid, station) in enumerate(stations, 1) :
		title = station[0]
		title = title[:max_title].strip()
		(width, height) = fnt.getsize(title)
		draw.line( (0, int(step *rid), max_W + 50, int(step *rid)), fill=(0,0,0), width=1)
		top 	= int(step *rid)-int(height / 2)-5
		bottom 	= top+height+10
		if ( rid % 3 ) == 0 :
			right = 75
		elif ( rid % 2 ) == 0 :
			right = 45
		else :
			right = 15
		draw.rectangle( (right, top, right+width+20, bottom), fill=bgcolor ) #, outline=(255,255,255)  )
		draw.text((right+10, top+1), title, font=fnt, fill=(50,50,50))
		
		# updating list of stations
		stations[rid-1] = (station[0], station[1], top, bottom, station[4])

	# Draw line
	draw.line((max_W+10,0, max_W+10,screensize[1]), fill=(0,0,0), width=7)

	# "Return" image
	return image

# Create TUNE picture which is INITIAL + line at top level which looks like a tune-line in old radio
#	Makes a copy of INITIAL image and adds line
#	Returns new image to display
#
def createTunePicture ( image, position ) :

	limage  = image.copy()
	draw 	= ImageDraw.Draw(limage)

	draw.line((0, position, screensize[0], position), fill=(255,255,255), width=3  )

	return limage

# Using START and STOP labels from STATIONS list return:
#	- Station to play
#	- None is no station under seekbar
#
def detectStation ( position ) :
	global stations

	position

	for (title, url, start, end, volshift) in stations :
		if position > start and position < end :
			return (title, url, start, end, volshift)

	return None

# Determines what User do want: to make VOLUME up or down and translates it to OMXplayer commands.
#	Returns:
#		- "+" or "-" as a command
#		- count of commands to give to player (as "Give to minus to player")
#		- value to store as current
#
def getVolumeGuide ( old_value, new_value ) :

	shift 			= new_value - old_value

	if shift < 0 :
		return (b"-", shift*-1, old_value+shift)
	elif shift > 0 :
		return (b"+", shift, old_value+shift)
	else :
		return (None, None, old_value)

# Thread to play music
#
#	Uses: omxplayer (have to be installed in system)
#	Uses global variable ancvolume which stores current VOLUME
#
#	Params:
#		- url 		= URL with stream from station
#		- vnt 		= Event which says "We have leave station's block on Tune panel"
#		- muteevent = Event which says "User push button MUTE"
#		- lock 		= Lock to be sure we are only who manage the player thread
#		- volshift	= From station list, tells us do we need to make sound louder or quiter than overage
#
#	Is VOID
#
def playerThread ( url, vnt, muteevent, lock, volshift ) :
	global encvolume		# This is our VOLUME from Encoder

	try :
		with lock :
			playarray = [
					'omxplayer', url, 
					'--vol', str((encvolume * 300) + volshift),	# why 300? on start we can setup volume in 1/100 db, but every click will shift by 3 db
					#'-o', 'alsa:hw:1,0',
					'-o', 'hdmi',
					'--no-osd'
				]
			# print (playarray)
			omxprocess = subprocess.Popen(playarray, stdin=subprocess.PIPE, stdout=None, stderr=None, bufsize=0) # https://www.raspberrypi.org/forums/viewtopic.php?t=23875
			sleep(0.5)
		current_value = encvolume
		#
		# While white line is on the Station's name, listen to MUTE and VOLUME regulators
		#
		while not vnt.is_set() :
			# Do we nned to change VOLUME?	
			(direction, circles, current_value) = getVolumeGuide ( current_value, encvolume )
			if direction is not None:
				for i in range(circles) :
					try :
						omxprocess.stdin.write(direction)
						sleep(0.1)
					except BrokenPipeError:
						print ("BrokenPipeError on Volume")
						pass
			# Do we need to pause / play thread?
			if muteevent.is_set() :
				with lock :
					try :
						omxprocess.stdin.write(b"p")
						sleep(0.1)
					except BrokenPipeError: 
						print ("BrokenPipeError on Mute")
						pass

				muteevent.clear()
			# Just to make some little pause which controll electrical problems
			sleep(0.1)
		#
		# Thanks, exiting from Player
		#
		vnt.clear()
		with lock :
			omxprocess.stdin.write(b'q')
	except BrokenPipeError:
		print ("BrokenPipeError on Play")
		pass
	except AttributeError :
		print ("AttributeError")
		pass
	except NameError :
		print ("NameError")
		pass

# JUDGE to understand is this one click True rotation OR system generates wrong direction
#	the idea is to use woters: left or right. Uses variable: judgeencoder = {'tune':[], 'volume':[]}
#
#	Params: Which one encoder, what direction to judge
#	Returns: L / R
#
def judgeRotation ( encoder, direction ) :
	global judgeencoder

	inarow = judgeencoder.get( encoder, [] )
	inarow.append( direction )
	inarow = inarow[-5:]
	judgeencoder.update( {direction:inarow} )

	l = 0
	r = 0

	for i in inarow :
		if i == "L" : 
			l = l + 1
		elif i == 'R' :
			r = r + 1
		else :
			pass

	if l > r:
		return "L"
	elif r > l :
		return "R"
	else :					# this is possible at start when len(inarow) in [2, 4]
		return direction

# CHANGES position of WHITE LINE on the screen by emulating real thing
#	is VOID
#	Uses global variable POSITION
#
def rotationScaleEncoder ( channel ) :
	global dt
	global clk
	global position
	global screensize

	step = 10

	if GPIO.input(dt) == GPIO.input(clk) :
		# User move position to the left
		if judgeRotation ( 'tune', 'L' ) != 'L' : return True
		if position < screensize[1] - step:
			position = position + step
		else :
			print ("End of scale")
	else :
		# User move position to right
		if judgeRotation ( 'tune', 'R' ) != 'R' : return True
		if position > step :
			position = position - step 
		else :
			print ("End of scale")

# CHANGES value of "encvolume" as user rotates encoder
#	is VOID
# 	Uses global variable 
def rotationVolumeEncoder ( channel ) :
	global vdt
	global vclk
	global encvolume

	if GPIO.input(vdt) == GPIO.input(vclk) :
		if judgeRotation ( 'volume', 'L' ) == 'L' : encvolume = encvolume - 1		
	else :
		if judgeRotation ( 'volume', 'R' ) == 'R' : encvolume = encvolume + 1

# SETS Event to tell to main Player Process that user push "Mute"
#
def pushMute ( channel ) :
	global muteevent
	muteevent.set()

# READING setings from file
#	Path to file is global variable
# 	Returns (position, encvolume)
#
def readSettings () :
	global encodersfile

	try :
		with open(encodersfile, "r") as jfile:
			(p, v) = json.load(jfile)
	except :
		return (0, -8)

	return (p, v)

# WRITES settings on exit to file
#	Path to file is global variable
#	Returns True on success
#
def writeSettings () :
	global encodersfile
	global position
	global encvolume

	try :
		with open(encodersfile, "w+") as jfile:
			json.dump((position, encvolume), jfile)
	except :
		return False

	return True

if __name__ == "__main__": 

	# Hide mouse pointer during programm is running
	nopinter = subprocess.Popen(['unclutter', '-idle', '0.1', '-root'], stdin=None, stdout=None, stderr=None, bufsize=0)

	# Initialize Radio main windows with TUNE picture
	cv2.namedWindow("MainWindow", cv2.WND_PROP_FULLSCREEN)
	cv2.setWindowProperty("MainWindow",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
	back 			= createInitialPicture ()
	current_station = None

	# Initialize Events and Locks
	vnt 			= threading.Event()		# This will stop player
	muteevent		= threading.Event()		# This will manage Pause / Play signal
	lock 			= threading.Lock()		# This allow us to be sure we are only one who manage input to player
	vnt.clear()
	muteevent.clear()

	# Initialize catchers of encoders and pins
	GPIO.add_event_detect(clk, 	GPIO.FALLING  , callback=rotationScaleEncoder, 	bouncetime=75)
	GPIO.add_event_detect(vclk, GPIO.FALLING  , callback=rotationVolumeEncoder, bouncetime=75)
	GPIO.add_event_detect(vpp,  GPIO.FALLING  , callback=pushMute, 				bouncetime=300)

	# Reading initial values from file
	otune = -100	# this is impossible so first picture will be drawn (we redraw only if old != new position)
	(position, encvolume) = readSettings ()

	# Starting main work
	try :
		while True :
			station 	= detectStation ( position )
			if station is not None :
				(title, url, start, end, volshift) = station
				if current_station != title :
					current_station = title
					vnt.clear()
					p_thread = threading.Thread( name=title, target=playerThread, args=(url, vnt, muteevent, lock, volshift), daemon=None)
					p_thread.start()
			else :
				vnt.set()
				current_station = None

			if otune != position :
				otune 		= position
				image 		= createTunePicture ( back, position )
				npimage 	= numpy.array(image.transpose(Image.ROTATE_270))
				cv_image 	= cv2.cvtColor(npimage, cv2.COLOR_RGB2BGR)
				cv2.imshow("MainWindow", cv_image)

			key = cv2.waitKey(30)
	except KeyboardInterrupt :
		vnt.set()					# Kill player if we need it
		sleep(0.3)					# Wait till player will exit completely
		writeSettings ()			# Write settings like position and encoders to file
		cv2.destroyAllWindows()		# Close VC windows
		print ("\n\nExiting. Position: {}, Volume: {}".format(position, encvolume) )
#
#
#	AUTHOR Paul A Katurov (katurov@gmail.com)